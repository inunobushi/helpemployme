﻿export class RecruiterModel {
    constructor(Company: String,
        TimeAtFirm: [
            { Years: number },
            { Months: number }
        ],
        Applicants_Interested_in: Array<String>) {
        this.Applicants_Interested_in = Applicants_Interested_in;
        this.Company = Company;
        this.TimeAtFirm = TimeAtFirm;

    }
    Company: String;
    TimeAtFirm: [
        { Years: number },
        { Months: number }
    ]
    Applicants_Interested_in: Array<String>;
}