﻿export class UserModel {

    constructor(
        firstName: string,
        lastName: string,
        username: string,
        email: string,
        password: string,
        passwordConfirm: string,
        location: [
            { city: string; },
            { state: string; },
            { prefecture: string; },
            { province: string; },
            { shire: string; },
            { department: string; },
            { region: string; },
            { country: string; },
            { zipCode: string; }
        ],
        profileImgUrl: string
    ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
        this.location = location;
        this.profileImgUrl = profileImgUrl;

    }
    firstName: string;
    lastName: string;
    username: string;
    email: string;
    password: string;
    passwordConfirm: string;
    location: [
        { city: string; },
        { state: string; },
        { prefecture: string; },
        { province: string; },
        { shire: string; },
        { department: string; },
        { region: string; },
        { country: string; },
        { zipCode: string; }
    ];
    profileImgUrl: string;

}