﻿export class CandidateModel {
    constructor(
        ResumeUrl: String,
        DesiredJobType: String,/*[multi-select] checkboxes // full-time, contract, intern, etc*/
        Level: String, /*[single-select] dropdown Junior, Mid-Level, Senior Developer, etc*/
        ExpertiseOrProficiencies: Array<Number>,/*[Multi-select] checkboxes //Java, Ruby, etc*/
        PreviousWorkExperience: [
            { JobTitle: String },
            { Company: String },
            { Location: String },
            {
                From: [
                    { Month: String },
                    { Year: String }
                ]
            },/*[single-select] dropdown*/
            {
                To: [
                    { Month: String },
                    { Year: String }
                ]
            },/*[single-select] dropdown*/
            { Description: String },/*TextArea*/
            { Salary: String },
            { ReasonForLeaving: String }/*TextArea*/
        ],
        PreferredIndustries: String,/*[Multi-select] checkboxes //Agriculture, IT, insurance, etc*/
        TimeFrame: String,/*[single-select] dropdown*/
        ConstraintCategories: String, /*[Multi-select] checkboxes*/
        ReasonsForConstraints: String /* TextArea.*/
    ) {
        this.ResumeUrl = ResumeUrl;
        this.DesiredJobType = DesiredJobType;
        this.Level = Level;
        this.ExpertiseOrProficiencies = ExpertiseOrProficiencies;
        this.PreviousWorkExperience = PreviousWorkExperience;
        this.PreferredIndustries = PreferredIndustries;
        this.TimeFrame = TimeFrame;
        this.ConstraintCategories = ConstraintCategories;
        this.ReasonsForConstraints = ReasonsForConstraints;

    }
    ResumeUrl: String;
    DesiredJobType: String;/*[multi-select] checkboxes // full-time, contract, intern, etc*/
    Level: String; /*[single-select] dropdown Junior, Mid-Level, Senior Developer, etc*/
    ExpertiseOrProficiencies: Array<Number>;/*[Multi-select] checkboxes //Java, Ruby, etc*/
    PreviousWorkExperience: [
        { JobTitle: String },
        { Company: String },
        { Location: String },
        {
            From: [
                { Month: String },
                { Year: String }
            ]
        },/*[single-select] dropdown*/
        {
            To: [
                { Month: String },
                { Year: String }
            ]
        },/*[single-select] dropdown*/
        { Description: String },/*TextArea*/
        { Salary: String },
        { ReasonForLeaving: String }/*TextArea*/
    ];
    PreferredIndustries: String;/*[Multi-select] checkboxes //Agriculture, IT, insurance, etc*/
    TimeFrame: String;/*[single-select] dropdown*/
    ConstraintCategories: String /*[Multi-select] checkboxes*/
    ReasonsForConstraints: String /* TextArea.*/
}