import './CSS/site.css';
import 'bootstrap';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';
import { BrowserRouter, HashRouter } from 'react-router-dom';
import App from './Routes/Routes';
import Store from './Store/store';

function renderApp() {
    // This code starts up the React app when it runs in a browser. It sets up the routing
    // configuration and injects the app into a DOM element.
    const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href')!;
    ReactDOM.render(
        <Provider store={Store}>
            <AppContainer>
                <App />
            </AppContainer>
        </Provider>,
        document.getElementById('react-app')
    );
}

renderApp();

// Allow Hot Module Replacement
if (module.hot) {
    module.hot.accept('./Routes/Routes', () => {
        {/*    routes = require<typeof RoutesModule>('./routes').routes;*/ }
        renderApp();
    });
}
