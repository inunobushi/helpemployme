import * as React from 'react';
import { BrowserRouter, Route, Switch, HashRouter } from 'react-router-dom';
import { Layout } from '../Components/Layout';
import { Home } from '../Components/Home';
import { FetchData } from '../Components/FetchData';
import { Counter } from '../Components/Counter';

export const App = (props: any) => {

    const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href')!;
    return (
        <BrowserRouter basename={baseUrl}>
            <Switch>
                <Layout>
                    <Route exact path='/' component={Home} />
                    <Route path='/counter' component={Counter} />
                    <Route path='/fetchdata' component={FetchData} />
                </Layout>
            </Switch>
        </BrowserRouter>
    )
}

export default App;
