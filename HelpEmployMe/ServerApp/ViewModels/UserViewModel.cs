﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace HelpEmployMe.ServerApp.ViewModels
{
    public class UserViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Resume { get; set; }
        public class Location
        {
            public string City { get; set; }
            public string State { get; set; }
            public string Prefecture { get; set; }
            public string Province { get; set; }
            public string Shire { get; set; }
            public string Department { get; set; }
            public string Region { get; set; }
            public string Country { get; set; }
            public string ZipCode { get; set; }

        }
        public string profileImgUrl { get; set; }
        
    }
}
